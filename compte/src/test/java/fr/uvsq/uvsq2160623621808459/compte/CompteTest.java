package fr.uvsq.uvsq2160623621808459.compte;

import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.Before;

public class CompteTest {
	
	Compte cb ;
	
	@Before
	public void setUp() {
		cb = new Compte(50);
	}
	
	// Test du constructeur

	@Test
	public void testCompte() {
		assertNotNull(cb) ;
	}
	
	// Constructeur avec param
	@Test
	public void testCompteParam()
	{
		assertEquals(cb.getSolde(), 50,0);
	}
	
	// Test de la fonction crediter
	@Test
	public void testCrediterPositif() throws MontantNegatifException {
		cb.crediter(50);
		assertEquals(cb.getSolde(),100,0);
	}
	
	// Verification du montant
	@Test(expected=MontantNegatifException.class)
	public void testCrediterNegatif() throws MontantNegatifException {
		cb.crediter(-2);
	}
	
	// Fonction debiter
	@Test
	public void testDebiterPositif() throws SoldeInsuffisantException, MontantNegatifException {
		cb.debiter(50);
		assertEquals(0,0,0);
	}
	
	// Montant négatif dans le débit
	@Test
	public void testDebiterNegatif() throws SoldeInsuffisantException  {
		try {
			cb.debiter(-2);
			fail();
		} catch (MontantNegatifException e)
		{
			e.getMessage();
		}
		
	}
	
	// Interdiction de découvert
	@Test
	public void testDebiterDecouvert() throws MontantNegatifException 
	{
		try {
			cb.debiter(100);
			fail();
		} catch (SoldeInsuffisantException e) {
			e.getMessage();
		}
		
	}
	
	// Test Get Solde
	@Test
	public void TestGetSolde()
	{
		assertEquals(50, cb.getSolde(),0);
	}
	
	// Test du virement
	@Test
	public void TestVirementVers1() throws MontantNegatifException, SoldeInsuffisantException
	{
		Compte cb2 = new Compte();
		cb.virementVers(cb2, 20);
		assertEquals(cb2.getSolde(), 20,0);
	}
	
	// Solde insuffisant pour virement
	@Test
	public void TestVirementVersEchouee() throws MontantNegatifException
	{
		Compte cb2 = new Compte() ;
		try {
			cb.virementVers(cb2, 530);
			fail();
		} catch (SoldeInsuffisantException e )
		{
			e.getMessage() ;
		}
	}
	
	// Montant négatif pour virement
	@Test
	public void TestVirementVersNegatif() throws SoldeInsuffisantException
	{
		Compte cb2 = new Compte() ;
		try {
			cb.virementVers(cb2, -15);
			fail();
		} catch (MontantNegatifException e )
		{
			e.getMessage() ;
		}
	}
	
	@Test
	public void testCrediter() {
		try
		{
			cb.crediter(-2);
		}
		catch(MontantNegatifException e)
		{
			e.getMessage();
		}
		
	}
	
}
