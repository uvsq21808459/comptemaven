package fr.uvsq.uvsq2160623621808459.compte;


import java.util.Scanner;


public class App 
{
	public static void main( String[] args ) {
		double montant = 0 ;
		Compte cb = new Compte();
		Compte cb2 = new Compte() ;
		Scanner sc = new Scanner(System.in);
		System.out.println(" Bienvenue sur l'espace de gestion de votre compte bancaire");
		String answer = "";
		do
		{
			System.out.println(" 1 : Consulter votre Solde ");
			System.out.println(" 2 : Créditer votre compte ");
			System.out.println(" 3 : Débiter votre compte ");
			System.out.println(" 4 : Virement ");
			System.out.println(" 0 : Quitter ");
			answer = sc.nextLine() ;
			switch(answer) 
			{
			case "1" : 
				System.out.println("Votre solde actuel est " +cb.getSolde());
				break;
			
			case "2" : 
				System.out.println(" Saisissez le montant désiré");
				while (!sc.hasNextDouble())
				{
				    System.out.println("Saisir un montant correct");
				    sc.next();
				}
				montant = sc.nextDouble() ;
				try {
				cb.crediter(montant);
				} catch (MontantNegatifException e) 
				{
					System.out.println(e.getMessage());
				}
				break ;
				
			case "3" :
			
				System.out.println(" Saisissez le montant désiré");
				while (!sc.hasNextDouble())
				{
				    System.out.println("Saisir un montant correct");
				    sc.next();
				}
				montant = sc.nextDouble() ;
				try {
				cb.debiter(montant);
				} catch (MontantNegatifException | SoldeInsuffisantException e) 
				{
					System.out.println(e.getMessage());
				}
				break ;
			
			case "4" :
			
				System.out.println(" Saisissez le montant désiré");
				while (!sc.hasNextDouble())
				{
				    System.out.println("Saisir un montant correct");
				    sc.next();
				}
				montant = sc.nextDouble() ;
				try {
				cb.virementVers(cb2, montant) ;
				} catch (MontantNegatifException | SoldeInsuffisantException e) 
				{
					System.out.println(e.getMessage());
				}
				break ;
			case "0" :
				System.out.println("Au revoir");
				System.exit(0);
			default :
			
				System.out.println("Veuillez refaire votre choix");
				answer = sc.nextLine();
			}
			answer = sc.nextLine() ;
		}while(!(answer == "0"));
		sc.close();
	}
}

