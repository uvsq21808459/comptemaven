package fr.uvsq.uvsq2160623621808459.compte;


/*
 * Auteur:
 * ABDOULAYE BALDE
 * MASSYL SELMI
 */

public class Compte {

	private double _solde;

	public Compte()
	{
		_solde = 0.0;
	}
	
	public Compte (double solde)
	{
		_solde = solde ;
	}

	public double getSolde()
	{
		return _solde;
	}

	public void crediter(double montant) throws MontantNegatifException
	{
		if (montant < 0)
		{
			throw new MontantNegatifException();
		}
		else
		{
			_solde += montant;
			System.out.println(montant+" a été ajouté avec succès sur votre Compte");
			System.out.println("Nouveau solde : "+this.getSolde());
		}

	}

	public void debiter(double montant) throws SoldeInsuffisantException, MontantNegatifException
	{
		if (montant < 0)
		{
			throw new MontantNegatifException();
		}
		else if( _solde - montant < 0)
		{
			throw new SoldeInsuffisantException();
		}
		else
		{
			_solde -= montant;
			System.out.println(" Un montant de "+montant+" a été débité de votre compte");
			System.out.println("Nouveau solde : " +this.getSolde());

		}
	}

	public void virementVers(Compte cb, double somme) throws MontantNegatifException, SoldeInsuffisantException
	{
		if (somme < 0)
		{
			throw new MontantNegatifException() ;
		}
		if(this.getSolde() - somme < 0)
		{
			throw new SoldeInsuffisantException() ;
		}
		else
		{
			cb.crediter(somme);
			this.debiter(somme);
		}
	}

}