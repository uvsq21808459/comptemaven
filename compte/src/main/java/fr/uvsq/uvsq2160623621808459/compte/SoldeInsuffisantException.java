package fr.uvsq.uvsq2160623621808459.compte;

@SuppressWarnings("serial")
public class SoldeInsuffisantException extends Exception {

	public SoldeInsuffisantException() {
		super("Votre solde est insuffisant");
	}

}
