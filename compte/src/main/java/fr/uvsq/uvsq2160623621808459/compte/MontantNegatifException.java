package fr.uvsq.uvsq2160623621808459.compte;

@SuppressWarnings("serial")
public class MontantNegatifException extends Exception{

	public MontantNegatifException() {
		super(" Le montant que vous avez saisi est négatif ");
	}

}
